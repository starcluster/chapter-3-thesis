"""
Provides functions to exploit the code written in C in generate_mk.c
"""
import sys

import ctypes
from functools import partial, lru_cache
import itertools

from mpmath import polylog
from mpmath import exp
from mpmath import log

import numpy as np

sys.path.append('../../chapter-2-thesis/code')
from utils import Color

try:
    lib = ctypes.cdll.LoadLibrary("./generate_mk_plates.so")
except OSError:
    raise RuntimeError(
        f"{Color['RED']}Failed to load the C library. It appears that the C code has not been compiled. Please make sure to compile the C code using 'make' before running this script. {Color['END']}"
    )

compute_coeff = lib.compute_coeff
compute_coeff.restype = None
compute_coeff.argtypes = [
    ctypes.c_double,  # kx
    ctypes.c_double,  # ky
    ctypes.c_int,  # alpha
    ctypes.c_int,  # beta
    ctypes.c_int,  # i
    ctypes.c_int,  # j
    ctypes.c_double,  # aho
    ctypes.c_double,  # k0
    ctypes.c_double,  # a
    ctypes.c_double,  # d
    ctypes.c_int,  # N
    ctypes.POINTER(ctypes.c_double),  # *c_re
    ctypes.POINTER(ctypes.c_double),  # *c_im
]
# void compute_coeff(double kx, double ky, int alpha, int beta, int i, int j, double aho, double k0, double a, double d, int N, double *c_re, double *c_im)


@lru_cache(maxsize=None)
def gamma(d):
    """Computes Γ(d)"""
    return -3 * (
        (
            log(1 + exp(-1j * d)) / d +
            + 1j * polylog(2, -exp(-1j * d)) / d**2 +
            polylog(3, -exp(-1j * d)) / d**3
        )
    )


@lru_cache(maxsize=None)
def wrapper_compute_coeff(kx, ky, alpha, beta, i, j, aho, k0, a, distance, N):
    """Wrappinf C function for memiosation"""
    c_re, c_im = ctypes.c_double(0.0), ctypes.c_double(0.0)
    compute_coeff(kx, ky, alpha, beta, i, j, aho, k0, a, distance, N, c_re, c_im)
    return c_re.value, c_im.value


def generate_mk_wrapper(k, distance, deltaB, deltaAB, k0, aho, N, a):
    """
    Compute the matrix Mk for a given set of parameters, uses memoization.

    Args:
        k (tuple): A tuple of integers representing the wave vector.
        distance (float): The distance between the plates, if 0 no plates.
        deltaB (float): Zeeman shift.
        deltaAB (float): Frequency detuning between A and B sites.
        k0 (float): The wave number.
        aho (float): Cut-off.
        N (int): The number of particles in the system.
        a (float): Lattice spacing.

    Returns:
        Mk (numpy.ndarray): A 4x4 complex-valued  matrix.
    """
    Mk = np.zeros((4, 4), dtype=complex)

    for i, j, alpha, beta in itertools.product(range(2), repeat=4):
        c_re, c_im = wrapper_compute_coeff(
            k[0], k[1], alpha, beta, i, j, aho, k0, a, distance, N
        )
        Mk[2 * j + beta, 2 * i + alpha] = c_re + 1j * c_im

    d = 1 / np.sqrt(2) * np.array([[1, 1j], [-1, 1j]])

    Mk = -6 * np.pi / k0 * Mk

    Mk1 = d @ Mk[0:2, 0:2] @ d.conj().T
    Mk2 = d @ Mk[2:4, 0:2] @ d.conj().T
    Mk3 = d @ Mk[0:2, 2:4] @ d.conj().T
    Mk4 = d @ Mk[2:4, 2:4] @ d.conj().T
    Mk = np.block([[Mk1, Mk3], [Mk2, Mk4]])

    if distance == 0:
        Mk[0, 0] += 2 * deltaB + 2 * deltaAB
        Mk[1, 1] += -2 * deltaB + 2 * deltaAB
        Mk[2, 2] += 2 * deltaB - 2 * deltaAB
        Mk[3, 3] += -2 * deltaB - 2 * deltaAB
    else:
        gamma_d = -1j+ gamma(distance)
        Mk[0, 0] += 2 * deltaB + 2 * deltaAB + gamma_d
        Mk[1, 1] += -2 * deltaB + 2 * deltaAB + gamma_d
        Mk[2, 2] += 2 * deltaB - 2 * deltaAB + gamma_d
        Mk[3, 3] += -2 * deltaB - 2 * deltaAB + gamma_d
    return Mk


def Hk(distance, deltaB, deltaAB, k0, aho, N, a):
    """Returns a partial function that generates the Hk matrix."""
    return partial(
        generate_mk_wrapper,
        distance=distance,
        deltaB=deltaB,
        deltaAB=deltaAB,
        k0=k0,
        aho=aho,
        N=N,
        a=a,
    )
