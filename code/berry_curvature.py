""" 
Author: Pierre Wulles
Credits: Pierre Wulles, Sergey Skipetrov
License: GNU GPL
Maintainer: Pierre Wulles
Email: pierre.wulles@lpmmc.cnrs.fr
Status: Phd Student
Laboratory: LPMMC -- Grenoble -- France
Date: 2024-02-22
Username: Starcluster
Description: Plotting bands on complex plane to classify this system in the paradigm of non hermitian topology.
Repository git: https://gitlab.com/starcluster
Python 3.11.6 (main, Nov 14 2023, 09:36:21) [GCC 13.2.1 20230801]
NumPy: 1.26.3
"""

import sys
import csv

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import generate_mk as gmk
import band_diagram as bd

sys.path.append("../../chapter-2-thesis/code")
import lattice_f as lf
import lattice_r as lr
import tex
import graphix
import bz
import chern 

tex.useTex()


def compute_ev_in_complex_plane(phy, num, rebake=True):
    """
    Draw band diagram of an hexagonal lattice between two plates of
    perfectible conductive metal separated by distance. If distance=0
    then there are no plates (free space).
    """
    k0 = phy["k0"]
    a = phy["a"]
    distance = phy["distance"]
    Δ_B = phy["Δ_B"]
    Δ_AB = phy["Δ_AB"]

    step = num["step"]

    brillouin_zone, _, _ = lf.generate_bz(a, step)

    (
        band_1,
        band_2,
        band_3,
        band_4,
        x_lattice,
        y_lattice,
        omegas,
        gammas,
        weightas,
        sigmas,
    ) = bd.band_diagram(brillouin_zone, distance, Δ_B, Δ_AB, a, k0, num)

    with open(f"../data/ev_mk_{Δ_B}_{Δ_AB}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k, l in zip(omegas, gammas):
            writer.writerow([k, l])

def gen_berry_curvature(phy, num):
    distance = phy["distance"]
    a = phy["a"]
    k0 = phy["k0"]
    Δ_B = phy["Δ_B"]
    Δ_AB = phy["Δ_AB"]
    
    step = num["step"]
    n_dots = num["n_dots"]
    aho = num["aho"]
    n_sum = num["n_sum"]
    
    BZ, border, Dk = lf.generate_bz(a, step)
    grid_berry_curvature = bz.square_bz(1.5,n_dots)

    dim = 4

    lr.display_lattice(np.concatenate((BZ,border)))
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig("../figs/BZ_for_axes.pdf")
    plt.show()
    
    # graphix.plot_lattice_field(gmk.Hk(distance, Δ_B, Δ_AB, k0, aho, n_sum, a), grid_berry_curvature, Dk, dim,f"",r"$k_x a$",r"$k_y a$")

    graphix.plot_lattice_field(gmk.Hk(distance, Δ_B, Δ_AB, k0, aho, n_sum, a), np.concatenate((BZ,border)), Dk, dim,f"",r"$k_x a$",r"$k_y a$")


def integrate_bc_on_arbitrary_region(phy,num,region,Dkx,Dky,dim,n):
    distance = phy["distance"]
    a = phy["a"]
    k0 = phy["k0"]
    Δ_B = phy["Δ_B"]
    Δ_AB = phy["Δ_AB"]
    
    step = num["step"]
    n_dots = num["n_dots"]
    aho = num["aho"]
    n_sum = num["n_sum"]

    integrated = 0

    for k in region:
        lf = chern.lattice_field_k(k[0], k[1], gmk.Hk(distance, Δ_B, Δ_AB, k0, aho, n_sum, a), Dkx, Dky, dim, n)
        integrated += lf

            
    return integrated 
        
    

if __name__ == "__main__":
    PHY = {"k0": 2 * np.pi * 0.05, "a": 1, "distance": 0, "Δ_B": 12, "Δ_AB": 0}

    NUM = {"n_sum": 80, "aho": 0.1, "step": 0.1, "n_dots" : 10}

    # gen_berry_curvature(PHY,NUM)
    # exit()

    dim = 4
    n = [0,1]

    n_radius_region = 100
    radius_region = np.linspace(0.01,1,n_radius_region)
    radius_region_eff = np.linspace(0.01,4*np.pi/3/np.sqrt(3),n_radius_region)
    integrated_on_region = []
    # for r in radius_region:
    #     BZ, border, Dk = lf.generate_bz(1/r, NUM["step"])
    #     BZ += 0.01
    #     try:
    #         ior = np.sum(integrate_bc_on_arbitrary_region(PHY,NUM,BZ,Dk[0],Dk[1],dim,n)/2/np.pi)
    #     except ValueError:
    #         print("error encoutered while integrating BC, repeating the previous value")
    #         ior = integrated_on_region[-1]
    #     print(ior)
    #     integrated_on_region.append(ior)

    # with open(f"../data/integrated_on_region.csv", mode="w", newline="") as csvf:
    #     writer = csv.writer(csvf)

    #     for k, l in zip(radius_region, integrated_on_region):
    #         writer.writerow([k, l])

    data = np.genfromtxt("../data/integrated_on_region.csv",delimiter=",")
    radius_region = data[:,0]
    integrated_on_region = data[:,1]
                         

    plt.plot(radius_region_eff, integrated_on_region,color="blue",lw=2)
    plt.axvline(x=PHY["k0"],color="red",lw=2,label=r"$|\mathbf{k}_1|=k_0$")
    plt.axvline(x=PHY["k0"]*np.sqrt(3)/2,color="black",lw=2,label=r"$|\mathbf{k}_1|=k_0\frac{\sqrt{3}}{2}$")
    plt.xlabel(r"$\textrm{Side of hexagon } k_1a$",fontsize=20)
    plt.ylabel(r"$\textrm{Integrated Berry curvature}$",fontsize=20)
    plt.legend()
    # plt.title("",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig("../figs/integrating_bc_on_different_region.pdf",format="pdf",bbox_inches='tight')

    
        

# grid_berry_curvature = bz.square_bz(1.5,n_dots)


    # compute_ev_in_complex_plane(PHY, NUM, rebake=True)
    # plot_ev_in_complex_plane(PHY, NUM, rebake=True)
