""" 
Author: Pierre Wulles
Credits: Pierre Wulles, Sergey Skipetrov
License: GNU GPL
Maintainer: Pierre Wulles
Email: pierre.wulles@lpmmc.cnrs.fr
Status: Phd Student
Laboratory: LPMMC -- Grenoble -- France
Date: 2024-02-22
Username: Starcluster
Description: Plotting bands on complex plane to classify this system in the paradigm of non hermitian topology.
Repository git: https://gitlab.com/starcluster
Python 3.11.6 (main, Nov 14 2023, 09:36:21) [GCC 13.2.1 20230801]
NumPy: 1.26.3
"""

import sys
import csv

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import generate_mk as gmk
import band_diagram as bd

sys.path.append("../../chapter-2-thesis/code")
import lattice_f as lf
import tex

tex.useTex()


def compute_ev_in_complex_plane(phy, num, rebake=True):
    """
    Draw band diagram of an hexagonal lattice between two plates of
    perfectible conductive metal separated by distance. If distance=0
    then there are no plates (free space).
    """
    k0 = phy["k0"]
    a = phy["a"]
    distance = phy["distance"]
    Δ_B = phy["Δ_B"]
    Δ_AB = phy["Δ_AB"]

    step = num["step"]

    brillouin_zone, _, _ = lf.generate_bz(a, step)

    (
        band_1,
        band_2,
        band_3,
        band_4,
        x_lattice,
        y_lattice,
        omegas,
        gammas,
        weightas,
        sigmas,
    ) = bd.band_diagram(brillouin_zone, distance, Δ_B, Δ_AB, a, k0, num)

    with open(f"../data/ev_mk_{Δ_B}_{Δ_AB}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k, l in zip(omegas, gammas):
            writer.writerow([k, l])


def plot_ev_in_complex_plane(phy, num, rebake=True):
    Δ_B = phy["Δ_B"]
    Δ_AB = phy["Δ_AB"]
    data = np.genfromtxt(
        f"../data/ev_mk_{Δ_B}_{Δ_AB}.csv", delimiter=",", encoding=None
    )
    omegas = data[:, 0]
    gammas = data[:, 1]

    plt.scatter(omegas, gammas, c=gammas, cmap="plasma")
    plt.axis((-100, 40, -10, 200))
    plt.xlabel(r"$-0.5\mathrm{Re}(\lambda)$", fontsize=20)
    plt.ylabel(r"$\mathrm{Im}(\lambda)$", fontsize=20)
    plt.title("", fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig(
        f"../figs/eigenvalues_mk_on_bz_{Δ_B}_{Δ_AB}.pdf",
        format="pdf",
        bbox_inches="tight",
    )


if __name__ == "__main__":
    PHY = {"k0": 2 * np.pi * 0.05, "a": 1, "distance": 0, "Δ_B": 0, "Δ_AB": 12}

    NUM = {"n_sum": 80, "aho": 0.1, "step": 0.01}

    # compute_ev_in_complex_plane(PHY, NUM, rebake=True)
    plot_ev_in_complex_plane(PHY, NUM, rebake=True)
