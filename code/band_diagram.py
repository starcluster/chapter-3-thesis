""" 
Author: Pierre Wulles
Credits: Pierre Wulles, Sergey Skipetrov
License: GNU GPL
Maintainer: Pierre Wulles
Email: pierre.wulles@lpmmc.cnrs.fr
Status: Phd Student
Laboratory: LPMMC -- Grenoble -- France
Date: 2024-02-22
Username: Starcluster
Description: Create the band diagram of an honeycomb lattice of point-scatterers resonators.
Repository git: https://gitlab.com/starcluster
Python 3.11.6 (main, Nov 14 2023, 09:36:21) [GCC 13.2.1 20230801]
NumPy: 1.26.3
"""

import sys

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from multiprocessing import Pool, Manager

import generate_mk as gmk

sys.path.append("../../chapter-2-thesis/code")
import lattice_f as lf
import lattice_r as lr
import tex

tex.useTex()

plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)


# TODO: parallelisation

def process_lattice(args):
    k, data, result_queue = args
    distance, delta_b, delta_ab, k0, aho, n_sum, a, band_1, band_2, band_3, band_4, omegas, gammas, weightas, sigmas = data
    mk = gmk.generate_mk_wrapper(k, distance, delta_b, delta_ab, k0, aho, n_sum, a)
    lambdas, vects = np.linalg.eig(mk)

    vects = [vects[:, i] for i in range(4)]
    gamma = [ga.imag for ga in lambdas]
    tuples = sorted(zip(lambdas, vects, gamma))

    omega, vects, gamma = map(list, zip(*tuples))

    weightas_state, sigmas_state = [], []
    for v in vects:
        weighta = np.linalg.norm(v[0]) ** 2 + np.linalg.norm(v[1]) ** 2
        sigma = np.linalg.norm(v[0]) ** 2 + np.linalg.norm(v[2]) ** 2
        weightas_state.append(weighta)
        sigmas_state.append(sigma)

    for i in range(4):
        weightas[i].append(weightas_state[i])
        sigmas[i].append(sigmas_state[i])

    omega = [-0.5 * om.real for om in omega]

    band_1.append(omega[0])
    band_2.append(omega[1])
    band_3.append(omega[2])
    band_4.append(omega[3])
    omegas += omega
    gammas += gamma

    result_queue.put((band_1[-1], band_2[-1], band_3[-1], band_4[-1], omegas[-1], gammas[-1], weightas, sigmas))

def band_diagram(lattice, distance, delta_b, delta_ab, a, k0, num):
    """
    Calculates the band diagram of a lattice.

    Args:
        lattice (ndarray): A numpy array of shape (N, 2) containing the k vectors.
        distance (float): Distance between plates.
        delta_b (float): Zeeman shift.
        delta_ab (float): Frequency detuning between sites.
        k0 (float): Wavenumber of light.
        num -> aho (float): cut-off term.
        num -> n_sum (int): Number of lattice sites.
        a (float): Lattice spacing.
    """
    n_sum = num["n_sum"]
    aho = num["aho"]

    x_lattice, y_lattice = lattice.T
    band_1, band_2, band_3, band_4 = [], [], [], []
    omegas, gammas = [], []
    weightas = [[] for _ in range(4)]
    sigmas = [[] for _ in range(4)]
    

    for k in lattice:
        mk = gmk.generate_mk_wrapper(k, distance, delta_b, delta_ab, k0, aho, n_sum, a)
        lambdas, vects = np.linalg.eig(mk)

        vects = [vects[:, i] for i in range(4)]
        gamma = [ga.imag for ga in lambdas]
        tuples = sorted(zip(lambdas, vects, gamma))

        omega, vects, gamma = map(list, zip(*tuples))

        weightas_state, sigmas_state = [], []
        for v in vects:
            weighta = np.linalg.norm(v[0]) ** 2 + np.linalg.norm(v[1]) ** 2
            sigma = np.linalg.norm(v[0]) ** 2 + np.linalg.norm(v[2]) ** 2
            weightas_state.append(weighta)
            sigmas_state.append(sigma)

        for i in range(4):
            weightas[i].append(weightas_state[i])
            sigmas[i].append(sigmas_state[i])

        omega = [-0.5 * om.real for om in omega]

        band_1.append(omega[0])
        band_2.append(omega[1])
        band_3.append(omega[2])
        band_4.append(omega[3])
        omegas += omega
        gammas += gamma

    return (
        band_1,
        band_2,
        band_3,
        band_4,
        x_lattice,
        y_lattice,
        omegas,
        gammas,
        weightas,
        sigmas,
    )

def band_diagram_parall(lattice, distance, delta_b, delta_ab, a, k0, num):
    """
    Calculates the band diagram of a lattice.

    Args:
        lattice (ndarray): A numpy array of shape (N, 2) containing the k vectors.
        distance (float): Distance between plates.
        delta_b (float): Zeeman shift.
        delta_ab (float): Frequency detuning between sites.
        k0 (float): Wavenumber of light.
        num -> aho (float): cut-off term.
        num -> n_sum (int): Number of lattice sites.
        a (float): Lattice spacing.
    """
    n_sum = num["n_sum"]
    aho = num["aho"]

    x_lattice, y_lattice = lattice.T
    band_1, band_2, band_3, band_4 = [], [], [], []
    omegas, gammas = [], []
    weightas = [[] for _ in range(4)]
    sigmas = [[] for _ in range(4)]

    manager = Manager()
    result_queue = manager.Queue()

    data = (distance, delta_b, delta_ab, k0, aho, n_sum, a, band_1, band_2, band_3, band_4, omegas, gammas, weightas, sigmas)

    args = [(k, data, result_queue) for k in lattice]

    with Pool() as pool:
        pool.map(process_lattice, args)

    result_list = []
    while not result_queue.empty():
        result = result_queue.get()
        # Faire quelque chose avec les résultats
        result_list.append(result)


    print(result_list)
    exit()
    
    omegas = result_list[-4]
    gammas = result_list[-3]


    return (
        band_1,
        band_2,
        band_3,
        band_4,
        x_lattice,
        y_lattice,
        omegas,
        gammas,
        weightas,
        sigmas,
    )


def plot_band_diagram(phy, num, rebake=True):
    """
    Draw band diagram of an hexagonal lattice between two plates of
    perfectible conductive metal separated by distance. If distance=0
    then there are no plates (free space).
    """
    k0 = phy["k0"]
    a = phy["a"]
    distance = phy["distance"]

    n_dots = num["n_dots"]

    n = 4 * int(n_dots / 3)
    lattice = lf.generate_path_bz(a, n_dots, "NE")
    fig, axes = plt.subplots(nrows=2, ncols=2)
    for ax, delta_b, delta_ab, lettre in zip(
        axes.flat, [0, 12, 0, 12], [0, 0, 12, -12], ["(a)", "(b)", "(c)", "(d)"]
    ):
        try:
            if rebake:
                raise OSError
            csv = np.genfromtxt(
                f"../data/omegas_{delta_b}_{delta_ab}_0.csv", delimiter=","
            )
            xprime, omegas, gammas = tuple(csv.transpose().tolist())
            csv2 = np.genfromtxt(
                f"../data/info_{delta_b}_{delta_ab}_0.csv", delimiter=","
            )
            n2, nn, m2, m3 = tuple(csv2.tolist())

        except OSError:
            _, band_2, band_3, _, _, _, omegas, gammas, _, _ = band_diagram(
                lattice, 0, delta_b, delta_ab, a, k0, num
            )
            xprime = list(range(len(omegas)))
            n2 = int(n_dots / 3) * 4
            nn = len(band_2)
            m2 = min(band_2[int(nn / 2) : nn])
            m3 = max(band_3[int(nn / 2) : nn])
            np.savetxt(
                f"../data/omegas_{delta_b}_{delta_ab}_0.csv",
                list(zip(xprime, omegas, gammas)),
                delimiter=",",
                fmt="%s",
            )
            np.savetxt(
                f"../data/info_{delta_b}_{delta_ab}_0.csv",
                list(zip([n2, nn, m2, m3])),
                delimiter=",",
                fmt="%s",
            )

        if distance == 0:
            im = ax.scatter(
                xprime,
                omegas,
                c=gammas,
                cmap="plasma",
                marker="o",
                s=0.2,
                norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
                zorder=2,
            )
        else:
            ax.scatter(xprime, omegas, c="gray", s=0.2)
            try:
                if rebake:
                    raise OSError
                csv = np.genfromtxt(
                    f"../data/omegas_{delta_b}_{delta_ab}_{distance}_plates.csv",
                    delimiter=",",
                )
                xprime, omegas, gammas = tuple(csv.transpose().tolist())
                csv2 = np.genfromtxt(
                    f"../data/info_{delta_b}_{delta_ab}_{distance}_plates.csv",
                    delimiter=",",
                )
                n2, nn, m2, m3 = tuple(csv2.tolist())

            except OSError:
                (
                    _,
                    band_2,
                    band_3,
                    _,
                    _,
                    _,
                    omegas,
                    gammas,
                    _,
                    _,
                ) = band_diagram(lattice, distance, delta_b, delta_ab, a, k0, num)

                xprime = list(range(len(omegas)))
                np.savetxt(
                    f"../data/omegas_{delta_b}_{delta_ab}_{distance}_plates.csv",
                    list(zip(xprime, omegas, gammas)),
                    delimiter=",",
                    fmt="%s",
                )
                np.savetxt(
                    f"../data/info_{delta_b}_{delta_ab}_{distance}_plates.csv",
                    list(zip([n2, nn, m2, m3])),
                    delimiter=",",
                    fmt="%s",
                )

            im = ax.scatter(
                xprime,
                omegas,
                c=gammas,
                cmap="plasma",
                marker="o",
                s=0.2,
                zorder=2,
                norm=matplotlib.colors.Normalize(vmin=0.001, vmax=1),
            )

        ax.axvline(
            x=n2 + n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axvline(
            x=n2 - n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axis((0, len(xprime), -200, 140))

        if lettre in ("(b)", "(c)"):
            ax.axhspan(m2, m3, alpha=0.5, color="gray")

        if lettre in ("(a)", "(b)"):
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=12)
        else:
            ax.set_xticks(ticks=[0, n, 2 * n], labels=[r"$M$", r"$\Gamma$", r"$K$"])
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
            ax.tick_params(labelsize=12)
        if lettre in ("(d)", "(b)"):
            ax.set_ylabel("")
            ax.set_yticks([])
        else:
            ax.set_ylabel(
                r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12
            )

        x_lettre = 0.02
        y_lettre = 0.92
        x_param = 0.65
        y_param = 0.18
        ax.text(x_lettre, y_lettre, s=f"${lettre}$", transform=ax.transAxes, zorder=4)
        s = (
            r"\noindent $\Delta_{\mathbf{B}}="
            + f"{delta_b}"
            + "\\\\ \\Delta_{AB}="
            + f"{delta_ab}$"
        )
        ax.text(x_param, y_param, s=s, transform=ax.transAxes, zorder=4)

    fig.subplots_adjust(wspace=0.03, hspace=0.12)
    cbar_ax = fig.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma/\Gamma_0$")
    plt.subplots_adjust(
        top=0.885, bottom=0.11, left=0.14, right=0.9, hspace=0.025, wspace=0.025
    )

    plt.savefig(
        f"../figs/band_diagram_{distance}.pdf", format="pdf", bbox_inches="tight"
    )


def plot_2cell(distance, k0, a,num):
    lattice = lf.generate_path_bz_folded(num["n_dots"])
    B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = band_diagram(
        lattice, distance, 0, 0, a, k0, num
    )
    xprime = [i for i in range(len(omegas))]
    f = plt.figure()
    ax = f.add_subplot(111)
    im = ax.scatter(
        xprime,
        omegas,
        c=gammas,
        cmap="plasma",
        marker="o",
        s=5,
        norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
        zorder=2,
    )
    # ax.axhspan(min(B2), max(B3), alpha=0.5, color="gray")
    ax.set_ylabel(r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=20)
    ax.set_xlabel(r"$\textrm{Quasimomentum}$ $\mathbf{k}$", fontsize=20)
    ax.axis((0, len(xprime), -200, 150))
    cbar_ax = f.add_axes([0.91, 0.11, 0.05, 0.7])
    clb = f.colorbar(im, cax=cbar_ax)
    cb_title = clb.ax.set_title(r"$\Gamma/\Gamma_0$",fontsize=20)
    cb_title.set_position([0.8, -1])
    clb.ax.tick_params(labelsize=20)
    n = 4 * num["n_dots"]

    ax.axvline(n // 5, color="black", linestyle="--")
    # ax.axvline(n*2,color="black",linestyle="--")
    ax.axvline(2 * n // 5, color="black", linestyle="--")
    ax.axvline(3 * n // 5, color="black", linestyle="--")
    ax.set_xticks(
        [0, n // 5, 2 * n // 5, 3 * n // 5, 4 * n // 5, n],
        [
            "$\widetilde{K}$",
            "$\widetilde{\Sigma}$",
            "$\Gamma$",
            "$\widetilde{T}$",
            "$\widetilde{K}$",
            "$\widetilde{M}$",
        ],
        fontsize=20,
    )
    ax.tick_params(axis='y', which='major', labelsize=20)
    plt.savefig(
        f"../figs/2cell_a{a}_d{distance}.pdf", format="pdf", bbox_inches="tight"
    )
    plt.show()


def plot_3D(phy,num):
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    delta_b = 0
    delta_ab = 12
    distance = 0
    
    BZ, border, dk = lf.generate_bz(phy["a"], step=0.05)
    lattice = np.concatenate((BZ,border))
    x,y = BZ.T

    B1,B2,B3,B4,x,y,omegas,gammas,_,_ = band_diagram(lattice,
                                                     distance, delta_b, delta_ab, phy["a"], phy["k0"], num)
    print(B1)

    ax.scatter(x,y,B2,color=plasma_purple)
    ax.scatter(x,y,B3,color=plasma_orange)

    plt.title("",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    ax.tick_params(axis='z', labelsize=20)
    # plt.zticks(fontsize=20)
    ax.elev = 8
    ax.azim = -46
    plt.savefig(f"../figs/BD_3D_db{delta_b}_dab{delta_ab}.pdf",format="pdf",bbox_inches='tight')
    plt.show()

    
    
if __name__ == "__main__":
    PHY = {"k0": 2 * np.pi * 0.05, "a": 1, "distance": 0}

    NUM = {
        "n_dots": 400,
        "n_sum": 80,
        "aho": 0.1,
    }


    plot_3D(PHY,NUM)
    exit()
    
    plot_2cell(distance=0, k0=2*np.pi*0.05, a=1,num=NUM)
    exit()

    plot_band_diagram(PHY, NUM, rebake=True)
    PHY["distance"] = 2
    plot_band_diagram(PHY, NUM, rebake=True)
    PHY["distance"] = 11
    plot_band_diagram(PHY, NUM, rebake=True)
