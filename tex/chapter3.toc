\babel@toc {british}{}\relax 
\contentsline {chapter}{\numberline {1}The honeycomb lattice between plates of metal}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Introduction}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Honeycomb atomic lattice in the free space}{6}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}The model}{6}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Band diagram of a honeycomb lattice in the free space}{7}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Width of the band gap}{10}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Topological properties of the band structure}{11}{subsection.1.2.4}%
\contentsline {section}{\numberline {1.3}Honeycomb atomic lattice in a Fabry-P\'{e}rot cavity}{13}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Green's function in a Fabry-P\'{e}rot cavity}{14}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Atom in a Fabry-P\'{e}rot cavity}{15}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Band diagram of a honeycomb lattice in a Fabry-P\'{e}rot cavity}{17}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Topological properties of the band structure}{18}{subsection.1.3.4}%
\contentsline {section}{\numberline {1.4}Conclusion}{19}{section.1.4}%
\contentsline {section}{\numberline {.1}Derivation of the formula for the width of the spectral gap}{20}{section.Alph0.1}%
