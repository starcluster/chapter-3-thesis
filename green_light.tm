<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <section|Maxwell equations>

  The equations describing the evolution of an electromagnetic field in time
  and space are derived from the works of James C. Maxwell [reference]. These
  equations establish a relationship between the electric field
  <math|<math-bf|E><around*|(|<math-bf|r>,t|)>> [V/m] and the magnetic field
  <math|<math-bf|B><around*|(|<math-bf|r>,t|)>> [W/m<math|<rsup|2>>]. They
  are formulated in a modern form using the divergence and curl operators:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<nabla\>\<cdummy\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|<frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>><eq-number><label|max-gauss>>>|<row|<cell|\<nabla\>\<cdummy\><math-bf|B><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|0<eq-number><label|max-thomson>>>|<row|<cell|\<nabla\>\<times\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\><math-bf|B><around*|(|<math-bf|r>,t|)>|\<partial\>t><eq-number><label|max-faraday>>>|<row|<cell|\<nabla\>\<times\><math-bf|B><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)><eq-number><label|max-ampere>>>>>
  </eqnarray*>

  Where <math|\<rho\><around*|(|<math-bf|r>,t|)>> [C/m<math|<rsup|3>>] is the
  total charge density and <math|<math-bf|J><around*|(|<math-bf|r>,t|)>> is
  the current density. These last two are linked by the equation:

  <\equation>
    <frac|\<partial\>\<rho\><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<nabla\>\<cdummy\><math-bf|J><around*|(|<math-bf|r>,t|)>=0<label|continuite>
  </equation>

  and <math|\<varepsilon\><rsub|0>=8.854\<times\>10<rsup|-12>> [F/m] is the
  free-space electric permittivity while <math|\<mu\><rsub|0>=4\<pi\>\<times\>10<rsup|-7>>
  is the free-space magnetic permeability. These two constants are related by
  the equation:

  <\equation>
    \<varepsilon\><rsub|0>\<mu\><rsub|0>=<frac|1|c<rsup|2>>
  </equation>

  where <math|c=3\<times\>10<rsup|-9>> [m/s] is the speed of light in vacuum.\ 

  <section|Wave equation in Fourier space>

  If we take the curl of <reference|max-faraday> we get:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<nabla\>\<times\>\<nabla\>\<times\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\>\<nabla\>\<times\><math-bf|B><around*|(|<math-bf|r>,t|)>|\<partial\>t><eq-number>>>|<row|<cell|<below|\<Rightarrow\>|using
    <reference|max-ampere>>\<nabla\><around*|(|\<nabla\>\<cdummy\><math-bf|E><around*|(|<math-bf|r>,t|)>|)>-\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\>|\<partial\>t><around*|(|\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>|)><eq-number>>>|<row|<cell|<below|\<Rightarrow\>|using
    <reference|max-gauss>>\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>>|)>-\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><rsup|2><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t<rsup|2>>-\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)><eq-number>>>|<row|<cell|\<Rightarrow\>\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>-<frac|1|c<rsup|2>><frac|\<partial\><rsup|2><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t<rsup|2>>>|<cell|=>|<cell|\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>+\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>>|)><eq-number>>>>>
  </eqnarray*>

  We now introduce the Fourier transform of the involved fields:

  <\equation>
    <math-bf|E><around*|(|<math-bf|r>,t|)>=<frac|1|2\<pi\>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><math-bf|E><around*|(|<math-bf|r>,\<omega\>|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>\<omega\>
  </equation>

  <\equation>
    <math-bf|B><around*|(|<math-bf|r>,t|)>=<frac|1|2\<pi\>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><math-bf|B><around*|(|<math-bf|r>,\<omega\>|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>\<omega\>
  </equation>

  We obtain:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<below|\<Rightarrow\>|using
    <reference|continuite>><around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>-\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|<math-bf|J><around*|(|<math-bf|r>,t|)>|i\<omega\>\<varepsilon\><rsub|0>>|)><eq-number>>>|<row|<cell|\<Rightarrow\><around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<mu\><rsub|0>i\<omega\><around*|(|<math-bf|J><around*|(|<math-bf|r>,t|)>+\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|<math-bf|J><around*|(|<math-bf|r>,t|)>|*k<rsub|0><rsup|2>>|)>|)><eq-number>>>>>
  </eqnarray*>

  Where we introduced <math|k<rsub|0>=\<omega\>/c>.

  <section|Green propagator>

  The solution of\ 

  <\equation>
    <around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|G><around*|(|<math-bf|r>,<math-bf|r><rprime|'>|)>=-\<delta\><around*|(|<math-bf|r>-<math-bf|r><rsup|<rprime|'>>|)>
  </equation>

  is given for spherical homogeneous and isotropic medium by\ 

  <\equation>
    G<rsub|0><rsup|\<pm\>><around*|(|r|)>=<frac|\<mathe\><rsup|\<pm\>i*k<rsub|0>r>|4\<pi\>r>
  </equation>

  If we apply on this last equation the operator
  <math|<with|font|Bbb|1>+<frac|\<nabla\>\<nabla\>|k<rsub|0><rsup|2>>> we
  get:

  \;

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<nabla\>\<nabla\>|k<rsub|0><rsup|2>>G<rsub|0><rsup|\<pm\>><around*|(|r|)>>|<cell|=>|<cell|>>>>
  </eqnarray*>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|2>>
    <associate|continuite|<tuple|5|1>>
    <associate|max-ampere|<tuple|4|1>>
    <associate|max-faraday|<tuple|3|1>>
    <associate|max-gauss|<tuple|1|1>>
    <associate|max-thomson|<tuple|2|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Maxwell
      equations> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Wave
      equation in Fourier space> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Green
      propagator> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>